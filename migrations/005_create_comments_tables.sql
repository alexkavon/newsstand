CREATE TABLE comments(
    id SERIAL NOT NULL PRIMARY KEY,
    comment TEXT NOT NULL,
    user_id INT NOT NULL REFERENCES users(id),
    post_id INT NOT NULL REFERENCES posts(id) ON DELETE CASCADE,
    reply_id INT REFERENCES comments(id) ON DELETE CASCADE,
    state postable_state DEFAULT 'visible',
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON comments
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

---- create above / drop below ----

DROP TABLE comments;
