CREATE TYPE postable_state AS ENUM ('hidden', 'visible');

CREATE TABLE posts(
    id SERIAL NOT NULL PRIMARY KEY,
    title VARCHAR(80) NOT NULL,
    description TEXT,
    url VARCHAR(255) UNIQUE,
    user_id INT NOT NULL REFERENCES users(id),
    state postable_state DEFAULT 'visible',
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON posts
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

---- create above / drop below ----

DROP TABLE posts;
DROP TYPE postable_state;
