.PHONY: migrate unmigrate rollback dev halt rs

migrate:
	TERN_CONFIG=migrations/tern.conf TERN_MIGRATIONS=migrations tern migrate

unmigrate:
	TERN_CONFIG=migrations/tern.conf TERN_MIGRATIONS=migrations tern migrate -d 0

rollback:
	TERN_CONFIG=migrations/tern.conf TERN_MIGRATIONS=migrations tern migrate -d -1

run:
	go run ./src/main.go

build:
	go build -o newsstand ./src/main.go

dev:
	podman compose up -d

halt:
	podman compose down

rs:
	podman compose up --build -d server

seed: unmigrate migrate
	go run ./seed/main.go
