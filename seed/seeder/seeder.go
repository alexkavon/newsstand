package seeder

import (
	"context"
	"log"

	"github.com/go-faker/faker/v4"
	"github.com/go-faker/faker/v4/pkg/options"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/alexkavon/newsstand/src/db"
	"gitlab.com/alexkavon/newsstand/src/models"
	"gitlab.com/alexkavon/newsstand/src/user"
)

type Seeder struct {
	dbconn *db.Db
}

func beginSeed(dbconn *db.Db, insertFunc func(context.Context, boil.ContextExecutor, boil.Columns) error) error {
	return insertFunc(context.Background(), dbconn.ToSqlDb(), boil.Infer())
}

func NewSeeder(dbconn *db.Db) *Seeder {
	options.SetGenerateUniqueValues(true)
	return &Seeder{dbconn}
}

func (s *Seeder) SeedUsers(num int) error {
	user.InitHooks()
	for i := 1; i <= num; i++ {
		u := &models.User{}
		u.Username = faker.Username()
		u.Secret = faker.Password()

		log.Printf("seeding user=%s", u.Username)
		// err := beginSeed(s.dbconn, u.Insert)
		err := u.Insert(context.Background(), s.dbconn.ToSqlDb(), boil.Infer())
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Seeder) SeedPosts(num int) error {
	// query users, pick random, generate num posts
	return nil
}
