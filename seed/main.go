package main

import (
	"log"

	"gitlab.com/alexkavon/newsstand/seed/seeder"
	"gitlab.com/alexkavon/newsstand/src/conf"
	"gitlab.com/alexkavon/newsstand/src/db"
)

func main() {
	config := conf.NewConf()
	database := db.NewDb(config)
	defer database.Close()
	// load seeder
	s := seeder.NewSeeder(database)
	// seed users
	err := s.SeedUsers(10)
	if err != nil {
		log.Fatal(err)
	}

	err = s.SeedPosts(5)
	if err != nil {
		log.Fatal(err)
	}
}
