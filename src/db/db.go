package db

import (
	"context"
	"database/sql"
	"log"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/jackc/pgx/v5/stdlib"

	"gitlab.com/alexkavon/newsstand/src/conf"
)

type Db struct {
	pool *pgxpool.Pool
}

func NewDb(config *conf.Conf) *Db {
	pool, err := pgxpool.New(context.Background(), config.Db.Url)
	if err != nil {
		log.Fatal(err)
	}

	var testquery string
	err = pool.QueryRow(context.Background(), "select 'Hello, PostgreSQL!'").Scan(&testquery)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Database connection pool created.", testquery)
	return &Db{pool}
}

func (d *Db) Conn() *pgxpool.Pool {
	return d.pool
}

func (d *Db) ToSqlDb() *sql.DB {
	return stdlib.OpenDBFromPool(d.pool)
}

func (d *Db) Close() {
	d.pool.Close()
}
