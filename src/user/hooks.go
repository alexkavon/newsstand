package user

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/alexkavon/newsstand/src/models"
)

func InitInsertHooks() {
	models.AddUserHook(boil.BeforeInsertHook, validateNewHook)
	// should always be last
	models.AddUserHook(boil.BeforeInsertHook, hashSecretHook)
}

func validateNewHook(ctx context.Context, exec boil.ContextExecutor, u *models.User) error {
	// validate user
	err := validation.ValidateStruct(u,
		validation.Field(&u.Username, validation.Required, validation.Length(3, 50)),
		validation.Field(&u.Secret, validation.Required, validation.Length(8, 128)),
	)
	if err != nil {
		return err
	}

	return nil
}

func hashSecretHook(ctx context.Context, exec boil.ContextExecutor, u *models.User) error {
	hashed, err := hashSecret(u.Secret)
	if err != nil {
		return err
	}
	u.Secret = hashed

	return nil
}
