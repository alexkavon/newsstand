package main

import (
	"gitlab.com/alexkavon/newsstand/src/conf"
	"gitlab.com/alexkavon/newsstand/src/db"
	"gitlab.com/alexkavon/newsstand/src/post"
	"gitlab.com/alexkavon/newsstand/src/server"
	"gitlab.com/alexkavon/newsstand/src/user"
)

func main() {
	// load config
	config := conf.NewConf()
	// connect database
	// start server
	database := db.NewDb(config)
	defer database.Close()
	s := server.NewServer(config, database)
	s.BuildUi()
	routers := []server.Routes{
		user.Routes,
		post.Routes,
	}
	for _, r := range routers {
		s.RegisterRoutes(r)
	}
	s.Serve()
}
