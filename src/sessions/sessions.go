package sessions

import (
	"net/http"
	"sync"

	"github.com/google/uuid"
)

type SessionValues map[string]any

type Session struct {
	id     string
	values SessionValues
	lock   *sync.Mutex
}

type SessionCtxKey string

var _sessions map[string]*Session

func InitStore() {
	_sessions = map[string]*Session{}
}

func NewSession(w http.ResponseWriter, values map[string]any) *Session {
	token := uuid.NewString()

	// set secure cookie in http.ResponseWriter
	// TODO make secure
	http.SetCookie(w, &http.Cookie{
		Name:  "session_token",
		Value: token,
	})

	// create session and store
	s := &Session{
		id:     token,
		values: SessionValues(values),
		lock:   &sync.Mutex{},
	}
	_sessions[token] = s
	return s
}

func GetSession(id string) (*Session, bool) {
	s, ok := _sessions[id]
	return s, ok
}

func (s *Session) Id() string {
	return s.id
}

func (s *Session) Get(key string) interface{} {
	s.lock.Lock()
	defer s.lock.Unlock()
	return s.values[key]
}

func (s *Session) Set(key string, value interface{}) bool {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.values[key] = value
	_sessions[s.id] = s
	return true
}

func (s *Session) Destroy(w http.ResponseWriter) {
	delete(_sessions, s.id)
	http.SetCookie(w, &http.Cookie{
		Name:  "session_token",
		Value: "",
	})
}
