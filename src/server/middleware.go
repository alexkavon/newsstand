package server

import "net/http"

func NewMiddlewares(middlewares ...func(http.Handler) http.Handler) []func(http.Handler) http.Handler {
	return middlewares
}
