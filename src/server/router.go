package server

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/alexkavon/newsstand/src/conf"
	"gitlab.com/alexkavon/newsstand/src/sessions"
)

type HandlerFunc func(s *Server) http.HandlerFunc

type ErrRes struct {
	Err            error  `json:"-"`
	HTTPStatusCode int    `json:"-"`
	StatusMessage  string `json:"status"`
	ErrorMessage   string `json:"error,omitempty"`
}

func (e *ErrRes) Render(w http.ResponseWriter, r *http.Request) error {
	// http.StatusCode(r, e.HTTPStatusCode)
	return nil
}

type Route struct {
	Name        string
	Method      string
	Path        string
	HandlerFunc HandlerFunc
	Middlewares []func(http.Handler) http.Handler
}

type Routes []Route

func NewRouter(config *conf.Conf) *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	sessions.InitStore()
	r.Use(sessions.StartSession)
	return r
}
