package server

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/alexkavon/newsstand/src/conf"
	"gitlab.com/alexkavon/newsstand/src/db"
)

type Server struct {
	Router *chi.Mux
	Db     *db.Db
	Config *conf.Conf
	Ui     Ui
}

func NewServer(config *conf.Conf, database *db.Db) *Server {
	return &Server{
		Router: NewRouter(config),
		Db:     database,
		Config: config,
		Ui:     NewUi(config),
	}
}

func (s *Server) BuildUi() {
	s.Ui.CompilePages(s.Config.Server.UiPath)
}

func (s *Server) RegisterRoutes(routes Routes) {
	for _, r := range routes {
		s.Router.With(r.Middlewares...).Method(r.Method, r.Path, r.HandlerFunc(s))
	}
}

func (s *Server) Serve() {
	http.ListenAndServe(":"+s.Config.Server.Port, s.Router)
}
