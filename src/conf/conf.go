package conf

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
)

type (
	Conf struct {
		cwd    string
		path   string
		Db     Db     `toml:"database"`
		Server Server `toml:"server"`
	}

	Db struct {
		Adapter  string `toml:"adapter"`
		User     string `toml:"user"`
		Secret   string `toml:"secret"`
		Hostname string `toml:"hostname"`
		Port     string `toml:"port"`
		DbName   string `toml:"dbname"`
		Url      string
	}

	Server struct {
		Hostname string `toml:"hostname"`
		Port     string `toml:"port"`
		UiPath   string `toml:"ui_path"`
	}
)

func NewConf() *Conf {

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	configpath := os.Getenv("NEWSSTAND_CONFIG_PATH")
	if configpath == "" {
		configpath = "./.newsstandrc.toml"
	}
	confpath := filepath.Clean(filepath.Join(cwd, configpath))
	log.Printf("Config file path: %s", confpath)

	c := Conf{
		cwd,
		confpath,
		Db{
			Hostname: "localhost",
		},
		Server{
			UiPath: cwd + "/ui",
		},
	}
	_, err = toml.DecodeFile(confpath, &c)
	if err != nil {
		log.Fatalln(err)
	}
	c.setDbUrl()
	log.Printf("Config loaded: %s", c)

	return &c
}

func (c *Conf) setDbUrl() {
	c.Db.Url = fmt.Sprintf(
		"%s://%s:%s@%s:%s/%s",
		c.Db.Adapter,
		c.Db.User,
		c.Db.Secret,
		c.Db.Hostname,
		c.Db.Port,
		c.Db.DbName,
	)
}

func (c *Conf) GetCwd() string {
	return c.cwd
}

func (c *Conf) GetPath() string {
	return c.path
}

func resolvePath(paths ...string) string {
	// TODO resolve file paths to cwd + partial or provided path
	return ""
}
