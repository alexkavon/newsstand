package post

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/alexkavon/newsstand/src/models"
)

func InitInsertHooks() {
	models.AddPostHook(boil.BeforeInsertHook, validateNewPostHook)
}

func validateNewPostHook(ctx context.Context, exec boil.ContextExecutor, p *models.Post) error {
	err := validation.ValidateStruct(p,
		validation.Field(&p.Title, validation.Required, validation.Length(10, 80)),
		validation.Field(&p.Description, validation.Max(5000)),
		validation.Field(&p.URL, validation.Length(3, 255), is.URL),
	)
	if err != nil {
		return err
	}

	return nil
}

func checkDescriptionLength() {
	// check if post/comment description is <= 5000 characters
}

func stripMarkdown() {
	// strip of specific tags or all tags
}

func checkBlacklist() {
	// check if URL is on blacklist
}
