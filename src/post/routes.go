package post

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/volatiletech/sqlboiler/v4/boil"
	. "github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/alexkavon/newsstand/src/models"
	"gitlab.com/alexkavon/newsstand/src/server"
	"gitlab.com/alexkavon/newsstand/src/sessions"
)

func init() {
	InitInsertHooks()
}

var Routes = server.Routes{
	server.Route{
		Name:        "Create",
		Method:      "GET",
		Path:        "/p/create",
		HandlerFunc: Create,
		Middlewares: server.NewMiddlewares(sessions.AuthSession),
	},
	server.Route{
		Name:        "Store",
		Method:      "POST",
		Path:        "/p",
		HandlerFunc: Store,
		Middlewares: server.NewMiddlewares(sessions.AuthSession),
	},
	server.Route{
		Name:        "Get",
		Method:      "GET",
		Path:        "/p/{id}",
		HandlerFunc: Get,
	},
}

func Create(s *server.Server) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.Ui.Render(w, r, "post/create", nil)
	}
}

func Store(s *server.Server) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var post models.Post
		post.Title = r.PostFormValue("title")
		post.Description.SetValid(r.PostFormValue("description"))
		post.URL.SetValid(r.PostFormValue("url"))

		// validate post
		// process post, look for spamminess, bad url
		// match post title with url title if provided
		// check title for tags: Ask NY, subway, crime, culture
		err := post.Insert(r.Context(), s.Db.ToSqlDb(), boil.Infer())
		if err != nil {
			log.Fatal("Insert Error", err)
		}

		// increment user points maybe
		// redirect to new post
		http.Redirect(w, r, fmt.Sprintf("p/%d", post.ID), http.StatusSeeOther)
	}
}

func Get(s *server.Server) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		postId, err := strconv.Atoi(chi.URLParam(r, "id"))
		if err != nil {
			log.Fatal(err)
		}

		post, err := models.Posts(
			Where("id = ?", postId),
			Load("User"),
			Load("Tags"),
		).One(r.Context(), s.Db.ToSqlDb())
		if err != nil {
			log.Fatal(err)
		}
		s.Ui.Render(w, r, "post/get", map[string]any{"post": post})
	}
}
